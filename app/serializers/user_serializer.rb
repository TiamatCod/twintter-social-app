class UserSerializer < ActiveModel::Serializer
  attributes :id, :name, :gender, :email, :contact_phone, :birthdate
  has_many :posts
end
