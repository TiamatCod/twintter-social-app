class Comment < ApplicationRecord
	#Relações
  belongs_to :post
  belongs_to :user
	
	#Esta é uma Self Join:
  has_many :replies, class_name: "Comment", foreign_key:"parent_comment_id"
  belongs_to :parent_comment, class_name: "Comment", optional: true  

  #Validação
  validates :content_text, presence: true, length:{maximum: 256} 

  def anti_spam
    if Comment.where(content_text: content_text, user_id: user.id).length > 5
      errors.add(:spam, "Você comentou isso antes")
    end
  end
end