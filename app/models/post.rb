class Post < ApplicationRecord
  #Relações
  belongs_to :user
  has_many :comments, dependent: :destroy
  has_many :like_posts, dependent: :destroy
  has_many :likes, through: :like_posts

end
