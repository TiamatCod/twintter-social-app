class CommentController < ApplicationController
	before_action :set_comment, only: [:show, :destroy, :update]
	load_and_authorize_resource

  def create
		@comment = Comment.new(comment_params.merge(user_id: current_user.id))
		if @comment.save
			render json: @comment, status: 201
		else
			render json: @comment.errors, status: 422
    end
  end

  def index
		@comments = Comment.all
		render json: @comments
  end

	def index_likes
		@likecomments = LikeComment.all
		render json: @likecomments
	end

  def show
		render json: @comment
  end

  def update
		if @comment.update(comment_params)
			render json: @comment, status: 201
		else
			render json: @comment.errors, status: 422
		end
  end

	def create_like
		@likecomment = LikeComment.new(like_params.merge(user_id: current_user.id))
		if @likecomment.save
			render json: @likecomment, status: 201
		else
			render json: @likecomment.errors, status: 422
		end
	end

  def destroy
		@comment.destroy
  end

	def set_comment
		@comment = Comment.find(params[:id])
	end
	
	def comment_params
		params.require(:comment).permit(
			:content_text,
			:post_id,
			:parent_comment_id)
	end

	def like_params
		params.require(:like).permit(
			:comment_id,
			:user_id
		)
	end
end
