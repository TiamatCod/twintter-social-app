class ApplicationMailer < ActionMailer::Base
  default from: 'twintter.social@gmail.com'
  layout 'mailer'
end
