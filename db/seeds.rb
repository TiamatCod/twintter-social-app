# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
require "faker"

#Preenchimento dos usuários
40.times.each do
  password = Faker::Crypto.md5[1..8].to_s
  
  User.create(name: Faker::Name.name, 
    email: Faker::Internet.email, 
    contact_phone: Faker::PhoneNumber.phone_number, 
    password: "123456",
    password_confirmation: "123456",
    gender: rand(0..1), 
    birthdate: Faker::Date.between(from: '1980-01-01', to: '2002-12-31'), 
    nickname: Faker::Name.first_name)
end


#Post e likes
#Preenchimento dos posts
40.times.each do
  Post.create(content_text: Faker::Lorem.paragraph_by_chars(number: 256, supplemental: false), user_id: rand(1..User.all.length))
end

  #Preenchimento dos LikePosts
80.times.each do
  LikePost.create(user_id: rand(1..User.all.length), post_id: rand(1..Post.all.length))
end
# end

#Comentários e likes

#Preenchimento dos Comments
80.times.each do |x|
  Comment.create(
    user_id: rand(1..User.all.length), 
    post_id: rand(1..Post.all.length), 
    content_text: Faker::Lorem.paragraph_by_chars(number: 256, supplemental: false),
    parent_comment_id: rand(0..1)==0 ? nil : rand(1..x)
  )

end
#Preenchimento dos LikeComments
50.times.each do
  LikeComment.create(user_id: rand(1..User.all.length), comment_id: rand(1..Comment.all.length))
end

50.times.each do
  Follow.create(follower_id: rand(1..User.all.length), followed_id: rand(1..User.all.length))
end
