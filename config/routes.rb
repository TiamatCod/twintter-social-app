Rails.application.routes.draw do
  post 'auth/signup'
  post 'auth/login' 
  post 'auth/forgot'
  post 'auth/forgot'
  post 'auth/reset_password'

  post 'comment/add_like', to: 'comment#create_like'
  post 'comment', to: 'comment#create'
  get 'comment/index_likes', to:'comment#index_likes'
  get 'comment/:id', to: 'comment#show'
  get 'comment', to: 'comment#index'
  put 'comment/:id', to: 'comment#update'
  delete 'comment/:id', to: 'comment#destroy'

  post 'posts/add_like', to: 'posts#create_like'
  post 'posts', to: 'posts#create'
  get 'posts/index_likes', to: 'posts#index_likes'
  get 'posts/:id', to: 'posts#show'
  get 'posts', to: 'posts#index'
  put 'posts/:id', to: 'posts#update'
  delete 'posts/:id', to: 'posts#destroy'

  post 'users', to: 'users#create'
  get 'users/:id', to: 'users#show'
  get 'users', to: 'users#index'
  put 'users/:id', to: 'users#update'
  delete 'users/:id', to: 'users#destroy'
end
